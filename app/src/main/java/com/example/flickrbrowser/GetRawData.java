package com.example.flickrbrowser;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

enum DownloadStatus {IDLE, PROCESSING, NOT_INITIALISED, FAIlED_OR_EMPTY, OK}

class GetRawData extends AsyncTask<String, Void, String> {
    private static final String TAG = "GetRawData";
    private DownloadStatus mDownLoadStatus;
    private final OnDownloadComplete mCallBack;

    interface OnDownloadComplete{
        public void onDownloadCompelete(String data,DownloadStatus status);
    }

    public GetRawData(OnDownloadComplete callback) {
        mCallBack = callback;
        this.mDownLoadStatus = DownloadStatus.IDLE;
    }

    @Override
    protected void onPostExecute(String s) {

//        super.onPostExecute(s);
        if(mCallBack != null)
        {
            mCallBack.onDownloadCompelete(s,mDownLoadStatus);
        }
        Log.d(TAG, "onPostExecute: ends ");
    }

    @Override
    protected String doInBackground(String... strings) {
        HttpURLConnection connection = null;
        BufferedReader reader = null;
        if (strings == null) {
            this.mDownLoadStatus = DownloadStatus.NOT_INITIALISED;
            return null;
        }


        try {

            mDownLoadStatus = DownloadStatus.PROCESSING;
            URL url = new URL(strings[0]);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            int response = connection.getResponseCode();
            Log.d(TAG, "doInBackground: Response code was" + response);

            StringBuilder result = new StringBuilder();
            reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line;
            while (null != (line = reader.readLine())) {
                result.append(line).append("\n");

            }
            mDownLoadStatus = DownloadStatus.OK;
            return result.toString();

        } catch (MalformedURLException e) {
            Log.e(TAG, "doInBackground:  Invalid Url" + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "doInBackground: IOException reading data" + e.getMessage());
        } catch (SecurityException e) {
            Log.e(TAG, "doInBackground: SecurityException needs permission" + e.getMessage());
        }
        finally {
            if(connection != null)
            {
                connection.disconnect();
            }
            if(reader != null)
            {
                try {

                    reader.close();

                }
                catch (IOException e)
                {
                    Log.d(TAG, "doInBackground: Error   closing reader "+ e.getMessage());
                }
            }
        }
        mDownLoadStatus = DownloadStatus.FAIlED_OR_EMPTY;
        return null;
    }
}
